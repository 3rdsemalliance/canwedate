# CanWeDate

This is a graded project for the Artificial Intelligence class, a part of the Computer Science course on the Poznań University of Technology. Feel free to use this project as a learning example or in any other way you see fit.

Co-author: [Oskar Kiliańczyk](https://www.gitlab.com/oskkil)
